﻿using UnityEngine;
using System.Collections;
using Player_Data_Collection;

public class Coin_Spawner : Player_Controller
{
    public float m_Spawn_Force;
    public float m_Event_Time;
    public float m_Coin_Frequency_Timer;
    public int m_Number_Of_Coin_Objects;
    private Transform m_Coin;
    private Rigidbody2D m_Coin_Rigidbody;
    private float rand;
    private float Timer;
    private float m_spawn_timer = 0;
    private Vector2 direction;
    private bool m_Spawn = false;
    private bool m_event_time_bool = false;
    private bool event_Active = false;
    private Player_ID player_ID;
    private Timer_Script m_Timer_Script;
    private GameObject m_Timer_object;
    private string[] m_Coin_Objects;


    // Use this for initialization
    void Start ()
    {
        m_Coin_Objects = new string[m_Number_Of_Coin_Objects];
        m_Timer_object = GameObject.Find("TrapTimerCounter");
        m_Timer_Script = m_Timer_object.GetComponent<Timer_Script>();

        for(int x = 0; x < m_Number_Of_Coin_Objects; x++)
        {
            int i = x + 1;
            m_Coin_Objects[x] = "Coin_" + i.ToString();
        }
    }

	void Update()
    {

        if (m_Timer_Script.m_Time_Left < 1 && m_Timer_Script.m_Event[0] == 1)
        {
            event_Active = true;
        }
        if (event_Active)
        {
            Timer += Time.deltaTime;
            m_spawn_timer += Time.deltaTime;

            if (Timer > m_Event_Time)
                m_event_time_bool = false;
            else
                m_event_time_bool = true;

            if (m_spawn_timer < m_Coin_Frequency_Timer)
                m_Spawn = false;
            else
                m_Spawn = true;
        }
    }

	// Update is called once per frame
	void FixedUpdate ()
    {
        rand = Random.Range(-1f, 0f);
        direction.x = rand;
        rand = Random.Range(-1f, 1.5f);
        direction.y = rand;
        int random_Coin = Random.Range(0, m_Number_Of_Coin_Objects);

        if (m_event_time_bool)
        {
            if (m_Spawn)
            {
                m_spawn_timer = 0;
                m_Coin = Instantiate(Resources.Load(m_Coin_Objects[random_Coin], typeof(Transform))) as Transform;
                m_Coin.position = transform.position;
                m_Coin_Rigidbody = m_Coin.gameObject.GetComponent<Rigidbody2D>();
                m_Coin_Rigidbody.AddForce(direction * m_Spawn_Force, ForceMode2D.Impulse);
            }
        }
        else
        {
            event_Active = false;
            m_spawn_timer = 0;
            Timer = 0;
        }
    }
}
