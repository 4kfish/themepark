﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Box_Prefab_Timer : MonoBehaviour
{

    public float m_Time_to_Spawn;
    public Transform m_Object;
    public int rand;
    private float Timer;
    private List<string> m_Object_Names = new List<string>();
    // Use this for initialization
    void Start ()
    {
        m_Object_Names.Add("Lighter_Pref");
        m_Object_Names.Add("SafetyNeedle_Pref");
        m_Object_Names.Add("Telephone_Pref");
        m_Object_Names.Add("Tennisball_Pref");
    }
	
	// Update is called once per frame
	void Update ()
    {
        Timer += Time.deltaTime;
        if(Timer > m_Time_to_Spawn)
        {
            rand = Random.Range(0, m_Object_Names.Count);
            m_Object = Instantiate(Resources.Load(m_Object_Names[rand], typeof(Transform))) as Transform;
            m_Object.position = transform.position;
            Timer = 0;
        }
	}
}
