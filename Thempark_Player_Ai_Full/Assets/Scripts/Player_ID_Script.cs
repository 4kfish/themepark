﻿using UnityEngine;
using System.Collections;
using Player_Data_Collection;

public class Player_ID_Script : MonoBehaviour {

    private GameObject m_Point_Code;
    private GameObject m_Game_Controller;
    private Game_Controller m_Game_Controller_Script;

    public Transform[] m_Players;
    public Player_ID[] m_PlayerID;
    public int player_Amount;

    // Use this for initialization
    void Start ()
    {
        DontDestroyOnLoad(gameObject);
        m_Point_Code = GameObject.Find("PointCode");

        m_Game_Controller = GameObject.Find("Scene");
        m_Game_Controller_Script = m_Game_Controller.GetComponent<Game_Controller>();
        player_Amount = (int)m_Game_Controller_Script.m_Player_Amount;

        m_PlayerID = new Player_ID[player_Amount];
        m_Players = new Transform[player_Amount];
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (m_Point_Code != null && m_Point_Code.GetComponent<PointCheck>().m_Change_Scene_Bool)
        {
            m_Players = m_Point_Code.GetComponent<PointCheck>().GetPlayerlist;
            for (int x = 0; x < player_Amount; x++)
            {
                Transform player = m_Players[x];
                m_PlayerID[x] = player.GetComponent<Player_Graphics>().Get_Player_ID;
            }
        }
	}
}