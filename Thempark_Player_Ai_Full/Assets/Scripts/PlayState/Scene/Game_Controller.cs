﻿using UnityEngine;
using System;
using System.Collections;
using Utility_Data_Collection;
using Player_Data_Collection;
using Random = UnityEngine.Random;


public class Game_Controller : MonoBehaviour
{
    public enum Rotational_Direction
    {
        Rotational_Direction_Left = -1,
        Rotational_Direction_Right = 1
    }

    public Player_Amount m_Player_Amount = Player_Amount.Player_Amount_Two;
    
    public Transform m_Track_Transform;
    public Rotational_Direction m_Rotational_Direction = Rotational_Direction.Rotational_Direction_Left;
    public float m_Track_Rotation_Speed;
    public float m_Track_Radius;
    public PointCheck m_PointCheck;
    public Camera_Rotation_Script m_CamRotation;

    //Newly ADDDED
    public Transform m_Player_Amount_Object;
    public PlayerAmount m_Player_Amount_Script;
    //

    public Vector3 Track_Center_Point { get { return m_Track_Transform.position; } }
    public float Track_Torque {
        get
        {
            float circumference = 2.0f * Mathf.PI * m_Track_Radius;
            float delta_Time = 360.0f / m_Track_Rotation_Speed * (int)m_Rotational_Direction;
            return circumference / delta_Time;
        }
    }

    private Transform[] m_Spawn_Point_Transforms;
    public Transform[] players;

    //NEWLY ADDED
    private int m_Player_List = 0;


    private void Awake ()
    {
        DontDestroyOnLoad(gameObject);
        GameObject[] spawn_Points = GameObject.FindGameObjectsWithTag("Spawn_Point");
        m_Spawn_Point_Transforms = new Transform[spawn_Points.Length];
        for (int i = 0; i < spawn_Points.Length; i++)
        {
            m_Spawn_Point_Transforms[i] = spawn_Points[i].GetComponent<Transform>();
        }
    }


    private void Start()
    {
        //NEWLY ADDED
        m_Player_Amount_Object = GameObject.Find("Player Amount").transform;

        if(m_Player_Amount_Script == null)
            m_Player_Amount_Script = m_Player_Amount_Object.GetComponent<PlayerAmount>();

        if(m_Player_Amount_Script.m_PlayerAmount == 2)
            m_Player_Amount = Player_Amount.Player_Amount_Two;
        else if (m_Player_Amount_Script.m_PlayerAmount == 3)
            m_Player_Amount = Player_Amount.Player_Amount_Three;
        else if (m_Player_Amount_Script.m_PlayerAmount == 4)
            m_Player_Amount = Player_Amount.Player_Amount_Four;
        ///
        

        players = new Transform[(int)m_Player_Amount];
        //for (int i = 0; i < (int)m_Player_Amount; i++)
        //{
        //    Transform new_Player = Instantiate(Resources.Load("Player", typeof(Transform))) as Transform;
        //    new_Player.position = m_Spawn_Point_Transforms[i].position;
        //    new_Player.rotation = m_Spawn_Point_Transforms[i].rotation;
        //    new_Player.GetComponent<Player_Movement>().Intitalize_Player(this, (Player_ID)i);
        //    new_Player.GetComponent<Player_Graphics>().Intitalize_Player(this, (Player_ID)i);
        //    players[i] = new_Player;
        //}


        //NEWLY ADDED
        if(m_Player_Amount_Script.m_Active[0])
        {
            Transform new_Player = Instantiate(Resources.Load("Player", typeof(Transform))) as Transform;
            new_Player.position = m_Spawn_Point_Transforms[3].position;
            new_Player.rotation = m_Spawn_Point_Transforms[3].rotation;
            new_Player.GetComponent<Player_Movement>().Intitalize_Player(this, (Player_ID)0);
            new_Player.GetComponent<Player_Graphics>().Intitalize_Player(this, (Player_ID)0);
            players[m_Player_List] = new_Player;
        }
        if (m_Player_Amount_Script.m_Active[1])
        {
            Transform new_Player = Instantiate(Resources.Load("Player", typeof(Transform))) as Transform;
            new_Player.position = m_Spawn_Point_Transforms[1].position;
            new_Player.rotation = m_Spawn_Point_Transforms[1].rotation;
            new_Player.GetComponent<Player_Movement>().Intitalize_Player(this, (Player_ID)1);
            new_Player.GetComponent<Player_Graphics>().Intitalize_Player(this, (Player_ID)1);
            m_Player_List++;
            players[m_Player_List] = new_Player;
        }
        if (m_Player_Amount_Script.m_Active[2])
        {
            Transform new_Player = Instantiate(Resources.Load("Player", typeof(Transform))) as Transform;
            new_Player.position = m_Spawn_Point_Transforms[2].position;
            new_Player.rotation = m_Spawn_Point_Transforms[2].rotation;
            new_Player.GetComponent<Player_Movement>().Intitalize_Player(this, (Player_ID)2);
            new_Player.GetComponent<Player_Graphics>().Intitalize_Player(this, (Player_ID)2);
            m_Player_List++;
            players[m_Player_List] = new_Player;
        }
        if (m_Player_Amount_Script.m_Active[3])
        {
            Transform new_Player = Instantiate(Resources.Load("Player", typeof(Transform))) as Transform;
            new_Player.position = m_Spawn_Point_Transforms[0].position;
            new_Player.rotation = m_Spawn_Point_Transforms[0].rotation;
            new_Player.GetComponent<Player_Movement>().Intitalize_Player(this, (Player_ID)3);
            new_Player.GetComponent<Player_Graphics>().Intitalize_Player(this, (Player_ID)3);
            m_Player_List++;
            players[m_Player_List] = new_Player;
        }
        //

        //Camera.main.GetComponent<Camera_Controller>().Intialize_Camera(players);
    }

    
    private void Update()
    {
        //NEWLY ADDED
        /*
        if(m_Player_Amount_Script.m_Active[0])
        {
            m_CamRotation.m_Players[0] = players[0];
        }
        if (m_Player_Amount_Script.m_Active[1])
        {
            m_CamRotation.m_Players[1] = players[1];
        }
        if (m_Player_Amount_Script.m_Active[2])
        {
            m_CamRotation.m_Players[2] = players[2];
        }
        if (m_Player_Amount_Script.m_Active[3])
        {
            m_CamRotation.m_Players[3] = players[3];
        }*/
        //

        float speed = m_Track_Rotation_Speed * Time.deltaTime * (int)m_Rotational_Direction;
        m_Track_Transform.Rotate(Vector3.forward * speed);
    }

}



