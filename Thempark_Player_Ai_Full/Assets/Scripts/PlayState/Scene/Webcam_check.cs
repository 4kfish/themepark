﻿using UnityEngine;
using System.Collections;

public class Webcam_check : MonoBehaviour
{
    private Transform m_Transform;

    
    IEnumerator Start()
    {
         m_Transform = transform;
        Renderer renderer = m_Transform.GetComponent<Renderer>();

        renderer.sortingOrder = 0;
        renderer.sortingLayerName = "Background";

       yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        { 
            WebCamTexture webcamTexture = new WebCamTexture();
            /* rawimage.texture = webcamTexture;
             rawimage.material.mainTexture = webcamTexture;*/
       renderer.material.mainTexture = webcamTexture;
       webcamTexture.Play();
  }
   else
   {
   } 
} 
    
    void Update()
    {
    }
}
