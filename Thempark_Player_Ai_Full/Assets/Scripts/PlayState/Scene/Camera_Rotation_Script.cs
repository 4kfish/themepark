﻿using UnityEngine;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;

public class Camera_Rotation_Script : MonoBehaviour {

    private Vector2 m_Velocity;
    public float m_SmoothTimeY;
    public float m_SmoothTimeX;

    /*
    public Transform m_PlayerOne;
    public Transform m_PlayerTwo;
    public Transform m_PlayerThree;
    public Transform m_PlayerFour;
    */

    public Transform[] m_Players;
    public float[] m_playersX;
    public float[] m_playersY;
    float posX;
    float posY;
    public Camera m_cam;
    private int m_mark = 0;

    public Transform m_Player_Amount_Object;
    public PlayerAmount m_Player_Amount_Script;


    void Start()
    {
        m_Player_Amount_Object = GameObject.Find("Player Amount").transform;
        m_Player_Amount_Script = m_Player_Amount_Object.GetComponent<PlayerAmount>();

        int playerAmount = m_Player_Amount_Script.m_PlayerAmount;

        m_Players = new Transform[playerAmount];
        m_playersX = new float[4];
        m_playersY = new float[4];
        m_cam.transform.SetParent(transform);
    }

    void FixedUpdate()
    {
        for(int i = 0; i < m_Player_Amount_Script.m_PlayerAmount; i++)
        {
            if (m_Player_Amount_Script.m_Active[i])
            {
                m_playersX[i] = m_Players[i].position.x;
                m_playersY[i] = m_Players[i].position.y;
            }
        }
        /*
        if (m_Player_Amount_Script.m_Active[m_mark])
        {
            m_playersX[m_mark] = m_Players[m_mark].position.x;
            m_playersY[m_mark] = m_Players[m_mark].position.y;
            m_mark++;
        }
        if (m_Player_Amount_Script.m_Active[m_mark])
        {
            m_playersX[m_mark] = m_Players[m_mark].position.x;
            m_playersY[m_mark] = m_Players[m_mark].position.y;
            m_mark++;
        }
        if (m_Player_Amount_Script.m_Active[m_mark])
        {
            m_playersX[m_mark] = m_Players[m_mark].position.x;
            m_playersY[m_mark] = m_Players[m_mark].position.y;
            m_mark++;
        }*/
        
        Array.Sort(m_playersX);
        Array.Sort(m_playersY);

        //Half distance between player 1 and 2
        float m_half_DistanceX = Mathf.Lerp(m_playersX[0], m_playersX[m_Player_Amount_Script.m_PlayerAmount], 0.5f);
        float m_half_DistanceY = Mathf.Lerp(m_playersY[0], m_playersY[m_Player_Amount_Script.m_PlayerAmount], 0.5f);

        //Blah blah
        Vector2 m_New_Camera_Pos = new Vector2(m_half_DistanceX, m_half_DistanceY);

        //Det här gör så att kameran laggar efter lite och gör en smooth transition när den ändrar håll.
        posX = Mathf.SmoothDamp(transform.position.x, m_New_Camera_Pos.x, ref m_Velocity.x, m_SmoothTimeX);
        posY = Mathf.SmoothDamp(transform.position.y, m_New_Camera_Pos.y, ref m_Velocity.y, m_SmoothTimeY);

        if (posY > -10.5f && posY < -8.5f)
        {
            if (posX > -6 && posX < +6)
            {
                transform.position = new Vector3(posX, posY, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, posY, transform.position.z);
            }
        }
        else
        {
            if (posX > -6 && posX < +6)
            {
                transform.position = new Vector3(posX, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            }
        }
    }
}
