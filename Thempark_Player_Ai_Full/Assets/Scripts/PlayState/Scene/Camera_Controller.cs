﻿using UnityEngine;
using System.Collections;
using Utility_Data_Collection;


public class Camera_Controller : MonoBehaviour
{

    public float m_Min_Camera_Size;
    public float m_Max_Camera_Size;
    public float m_Resize_Speed;
    public float m_Movement_Speed;
    public float m_Player_Edge_Padding;
    public float m_Camera_Offset_Y;

    private Transform m_Transform;
    private Transform[] m_Player_Transforms;
    private Camera m_Camera;
    private float m_Camera_Size_Max;
    private float m_Camera_Size_Min;
    

    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Camera = GetComponent<Camera>();
    }


    private void Start ()
    {
        m_Camera.orthographicSize = m_Min_Camera_Size;
        m_Camera_Size_Max = set_Camera_Size(m_Max_Camera_Size, m_Camera);
        m_Camera_Size_Min = set_Camera_Size(m_Min_Camera_Size, m_Camera);
    }


    private void Update ()
    {
        //Vector2 first_Player = m_Player_Transforms[0].position;
        //Vector2 last_Player = m_Player_Transforms[0].position;

        //m_Transform.position = set_Camera_Position(first_Player, last_Player, m_Transform.position, m_Camera_Offset_Y, m_Movement_Speed);

        //Player_Order(m_Player_Transforms, out first_Player, out last_Player);

        //if (On_Camera_Size_Change(first_Player, last_Player))
        //{
        //    m_Transform.position = set_Camera_Position(first_Player, last_Player, m_Transform.position,
        //        m_Camera_Offset_Y, m_Movement_Speed);
        //}
        //else
        //{
        //    float offset_X = (m_Camera_Size_Max / 2.0f) - m_Player_Edge_Padding;
        //    m_Transform.position += (Vector3.right * offset_X);
        //}
    }


    public void Intialize_Camera(Transform[] player_Transforms)
    {
        m_Player_Transforms = new Transform[player_Transforms.Length];

        for (int i = 0; i < player_Transforms.Length; i++)
        {
            m_Player_Transforms[i] = player_Transforms[i];
        }
    }


    private void Player_Order(Transform[] players, out Vector2 first_Pos, out Vector2 last_Pos)
    {
        var first_Player = players[0];
        var last_Player = players[0];

        for (int i = 1; i < m_Player_Transforms.Length; i++)
        {
            if (first_Player.position.x < m_Player_Transforms[i].position.x)
            {
                first_Player = m_Player_Transforms[i];
            }
            else if (last_Player.position.x > m_Player_Transforms[i].position.x)
            {
                last_Player = m_Player_Transforms[i];
            }
        }

        first_Pos = first_Player.position;
        last_Pos = last_Player.position;
    }


    private bool On_Camera_Size_Change(Vector2 first_Player, Vector2 last_Player)
    {
        float distance_X = Mathf.Abs(first_Player.x - last_Player.x) + m_Player_Edge_Padding;
        float camera_Width = get_Camera_Width(m_Camera);

        if (distance_X > camera_Width && distance_X < m_Camera_Size_Max && distance_X > m_Camera_Size_Min)
        {
            // changing camera size
            float width = Math_Ex.Const_Lerp(camera_Width, distance_X, m_Resize_Speed, Time.deltaTime);
            m_Camera.orthographicSize = set_Camera_Size(width, m_Camera);
        }
        else if (distance_X > m_Camera_Size_Max)
        {
            if (camera_Width < m_Camera_Size_Max)
            {
                float width = Math_Ex.Const_Lerp(camera_Width, m_Camera_Size_Max, m_Resize_Speed, Time.deltaTime);
                m_Camera.orthographicSize = set_Camera_Size(width, m_Camera);
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    private float get_Camera_Width(Camera camera)
    {
        var height = 2f * camera.orthographicSize;
        return height * camera.aspect;
    }

    private float set_Camera_Size(float width, Camera camera)
    {
        return width / camera.aspect;
    }

    private Vector3 set_Camera_Position(Vector2 first_Player, Vector2 last_Player,
        Vector2 current_Position, float camera_Offset_Y, float movement_Speed)
    {
        var normal = (first_Player - last_Player).normalized;
        var distance = Vector3.Distance(first_Player, last_Player) / 2.0f;
        var new_Position = last_Player + (normal * distance);
        var ref_Velocity = 0.0f;

        new_Position.x = Mathf.SmoothDamp(current_Position.x, new_Position.x,
            ref ref_Velocity, movement_Speed * Time.deltaTime);
        new_Position.y = Mathf.SmoothDamp(current_Position.y, new_Position.y + camera_Offset_Y,
            ref ref_Velocity, movement_Speed * Time.deltaTime);

        return new Vector3(new_Position.x, new_Position.y, -1.0f);
    }


}



