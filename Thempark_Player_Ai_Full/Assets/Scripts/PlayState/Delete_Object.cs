﻿using UnityEngine;
using System.Collections;

public class Delete_Object : MonoBehaviour
{
    public float m_Delete_Time;
    // Use this for initialization
    void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
        StartCoroutine(deleteSmoke());
	}

    IEnumerator deleteSmoke()
    {
        yield return new WaitForSeconds(m_Delete_Time);
        Destroy(gameObject);
    }
}
