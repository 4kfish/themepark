﻿using UnityEngine;
using System.Collections;

public class Track_Mechanic : MonoBehaviour
{

    public float m_Rotation_Angle;

    public float get_Rotational_Velocity
    {
        get
        {
            float circumference = 2.0f * Mathf.PI * m_Inner_Radius;
            float delta_Time = 360.0f / m_Rotation_Angle;
            return circumference / delta_Time;
        }
    }

    private Transform m_Transform;
    private float m_Inner_Radius;


    private void Awake ()
    {
        m_Transform = transform;
    }


    private void Start()
    {
        RaycastHit2D[] hit_Info = Physics2D.RaycastAll(m_Transform.position, m_Transform.right);

        for (int i = 0; i < hit_Info.Length; i++)
        {
            if (hit_Info[i].transform == m_Transform)
            {
                m_Inner_Radius = hit_Info[i].distance;
                break;
            }
        }
    }

    
    private void Update()
    {
        m_Transform.Rotate(transform.forward * m_Rotation_Angle * Time.deltaTime);
    }


}
