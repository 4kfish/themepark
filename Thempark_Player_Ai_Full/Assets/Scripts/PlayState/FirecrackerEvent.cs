﻿using UnityEngine;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;
using UnityEngine.UI;

public class FirecrackerEvent : MonoBehaviour
{
    public float m_Event_Time;

    private Transform m_Firecracker;
    private Transform m_Colliding_Transform;
    private Timer_Script m_Timer_Script;
    private PointCheck m_PointCheck;
    private Transform[] m_Points_List;
    private float Timer;
    private bool event_Active = false;
    private bool m_Spawn = true;
    private Vector3 m_Above_Head;
    private Player_ID m_PlayerID;

    // Use this for initialization
    void Start ()
    {
        m_Timer_Script = GameObject.Find("TrapTimerCounter").GetComponent<Timer_Script>();
        m_PointCheck = GameObject.Find("PointCode").GetComponent<PointCheck>();
        m_Points_List = new Transform[m_PointCheck.player_Amount];
        m_Above_Head = new Vector3(0, 4);
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_Points_List = m_PointCheck.m_Points_List;

        if (m_Timer_Script.m_Time_Left < 1 && m_Timer_Script.m_Event[0] == 2)
        {
            event_Active = true;
        }
        //Sort the list to floats

        float[] pointsFloatList;
        pointsFloatList = new float[m_PointCheck.player_Amount];
        for (int x = 0; x < m_PointCheck.player_Amount; x++)
        {
            pointsFloatList[x] = m_Points_List[x].GetComponent<Image>().fillAmount;
        }

        Array.Sort(pointsFloatList, m_Points_List);

        if (event_Active && m_Spawn)
        {
            m_Firecracker = Instantiate(Resources.Load("Firecracker", typeof(Transform))) as Transform;
            m_Firecracker.position = m_Points_List[0].transform.position/* + m_Above_Head*/;
            m_Firecracker.SetParent(m_Points_List[0]);
            m_Firecracker.rotation = m_Points_List[0].rotation;
            Debug.Log("lol");
            
            //Add OnCollisionEnter2D and make the colliding player get the firecracker;
            //Add timer so that the previous player can't get the cracker instantly;
            //
            m_Spawn = false;
        }
        if(event_Active && m_Firecracker != null)
        {
            StartCoroutine(explosionTime());
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            m_PlayerID = col.gameObject.GetComponent<Player_Graphics>().Get_Player_ID;
            m_Colliding_Transform = col.transform;
            if (event_Active)
            {
                m_Firecracker.position = col.transform.position + m_Above_Head;
                m_Firecracker.SetParent(col.transform);
                m_Firecracker.rotation = col.transform.rotation;
            }
        }
    }

    IEnumerator explosionTime()
    {
        yield return new WaitForSeconds(7.5f);
        Destroy(m_Firecracker.gameObject);
        m_PointCheck.m_Points_List[(int)m_PlayerID].GetComponent<Image>().fillAmount -= 0.002857142857f;
    }

    
}
