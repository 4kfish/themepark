﻿using UnityEngine;
using System;
using System.Collections;

[AddComponentMenu("Scripts/Ragdoll/Ragdoll Controller")]
[RequireComponent(typeof(LineRenderer))]


public class Ragdoll_Controller : MonoBehaviour
{

    public Material m_Segment_Material;
    public float m_Ragdoll_Length;
    public float m_Ragdoll_Width_Start;
    public float m_Ragdoll_Width_End;
    [Range(2, 20)]
    public int m_Segments;
    public Vector2 m_Local_Center_Point;
    [Range(0.0f, 359.0f)]
    public float m_Start_Angle;
    public bool m_Limit_Rotation;
    public float m_Min_Rotation;
    public float m_Max_Rotation;
    public float m_Gravity_Scale;

    public Transform first;
    public Transform second;

    public Vector2 Start_Position { get { return (Vector2) m_Transform.position + m_Local_Center_Point; } }
    public Vector2 End_Position { get { return (Vector2)m_Transform.position + m_Line_Points[m_Line_Points.Length - 1]; } }

    private Transform m_Transform;
    private LineRenderer m_Line_Renderer;
    private Vector2[] m_Line_Points;


    private void Awake ()
    {
        m_Transform = transform;
        m_Line_Renderer = m_Transform.GetComponent<LineRenderer>();
    }


    private void Start ()
    {
        m_Line_Renderer.SetWidth(m_Ragdoll_Width_Start, m_Ragdoll_Width_End);
        m_Line_Renderer.SetVertexCount(m_Segments);
        m_Line_Renderer.material = m_Segment_Material;

        m_Line_Points = new Vector2[m_Segments];
        Vector2 direction = Angled_Unit_Vector(m_Start_Angle);
        float part_Length = m_Ragdoll_Length / m_Segments;

        for (int i = 0; i < m_Segments; i++)
        {
            Vector2 position = m_Local_Center_Point + (direction * part_Length * i);
            m_Line_Points[i] = position;
            m_Line_Renderer.SetPosition(i, position);
        }
    }


    private void Update ()
    {
        
    }


    private Quaternion Clamp_Rotation_Axis(Quaternion rotation, float min, float max)
    {
        rotation.x /= rotation.w;
        rotation.y /= rotation.w;
        rotation.z /= rotation.w;
        rotation.w = 1.0f;

        float angle_Z = 2.0f * Mathf.Rad2Deg * Mathf.Atan(rotation.z);

        angle_Z = Mathf.Clamp(angle_Z, min, max);

        rotation.z = Mathf.Tan(0.5f * Mathf.Deg2Rad * angle_Z);

        return rotation;
    }


    private Vector2 Angled_Unit_Vector(float angle)
    {
        var angle_X = Mathf.Sin(angle * Mathf.Deg2Rad);
        var angle_Y = Mathf.Cos(angle * Mathf.Deg2Rad);
        return new Vector2(angle_X, angle_Y);
    }

}
