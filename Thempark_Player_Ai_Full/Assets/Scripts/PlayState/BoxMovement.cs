﻿using UnityEngine;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;
public class BoxMovement : MonoBehaviour {

    //TODO! Titta igenom och skriv om ifall du tycker att jag gjort fel någonstans eller om något kan förbättras
    public float m_Speed;
    public float m_Ground_Check_offset;
    public LayerMask m_Track_Layer_Mask;
    public Transform m_Track_Transform;
    
    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody;
    private BoxCollider2D m_Box_Collider;
    private Track_Mechanic m_Track_Mechanic;
    private int m_Player_Direction;
    private float m_Diagonal_Distance;
    private bool m_Is_Grounded, m_Jump, m_Is_Jumping, m_moveLeft, m_moveRight;

    Color m_color;



    private void Awake()
    {
        m_Transform = transform;
        m_Rigidbody = m_Transform.GetComponent<Rigidbody2D>();
        m_Box_Collider = m_Transform.GetComponent<BoxCollider2D>();
        m_Track_Transform = GameObject.Find("Track").transform;
        m_Track_Mechanic = m_Track_Transform.GetComponent<Track_Mechanic>();
    }

    private void Start()
    {

        transform.SetParent(m_Track_Transform.transform);
        m_Rigidbody.freezeRotation = true;
        m_Rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
        m_Rigidbody.WakeUp();
        m_Player_Direction = 1;

    }


    private void Update()
    {
       m_Transform.rotation = Math_Ex.get_Look_Rotation(m_Transform.position, m_Track_Transform.transform.position);
        
        
    }


    private void FixedUpdate()
    {
        if (transform.parent != null && transform.parent.parent != null)
        {
            transform.SetParent(m_Track_Transform.transform);
        }

        Ground_Physics_Cast(m_Is_Grounded);

        if (m_Is_Grounded && !m_Is_Jumping)
        {
            m_Rigidbody.velocity = -m_Transform.up * m_Track_Mechanic.get_Rotational_Velocity;
            //   m_Rigidbody.velocity = m_Transform.right * m_Player_Direction * m_Speed * Input.GetAxisRaw("Horizontal");
        }
        
    }


    void OnCollisionEnter2D(Collision2D Collider)
    {
        m_Rigidbody.gravityScale = 0;
        m_Rigidbody.isKinematic = false;
        if (Collider.gameObject.tag == "Player One")
        {
            Collider.transform.position = new Vector3(Collider.transform.position.x + 0.4f, Collider.transform.position.y + 0.2f, Collider.transform.position.z);
        }
        else if (Collider.gameObject.tag == "Player Two")
        {
            Collider.transform.position = new Vector3(Collider.transform.position.x + 0.4f, Collider.transform.position.y + 0.2f, Collider.transform.position.z);
        }
    }

    void OnCollisionExit2D(Collision2D Collider)
    {
        m_Rigidbody.gravityScale = 1;
    }



    

        private void Ground_Physics_Cast(bool prev_Grounded)
        {
            float width = m_Box_Collider.bounds.size.x;
            Vector2 origin = m_Transform.position + (m_Transform.up * width);
            RaycastHit2D hit_Info = Physics2D.CircleCast(origin, width, -m_Transform.up, width + m_Ground_Check_offset, m_Track_Layer_Mask);

            if (hit_Info.collider != null)
            {
                m_Is_Grounded = true;
            }
            else
            {
                m_Is_Grounded = false;
            }

            if (!prev_Grounded && m_Is_Grounded)
            {
                m_Is_Jumping = false;
            }
        }


        private Vector2 get_Movement_Velocity()
        {
            Vector2 velocity_Up = new Vector2
                (
                m_Rigidbody.velocity.x / m_Transform.up.x,
                m_Rigidbody.velocity.y / m_Transform.up.y
                );
            Vector2 velocity_Forward = m_Transform.right * Input.GetAxisRaw("Horizontal") * m_Speed;

            return velocity_Forward + velocity_Up;
        }


        [Serializable]
        public class Movement_Input
        {
            public float m_Speed;
            public float m_Jump_Height;
        }

        [Serializable]
        public class Movement_Settings
        {
            public float m_Ground_Check_offset;
            public LayerMask m_Track_Layer_Mask;
        }

        

}
