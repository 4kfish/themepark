﻿using UnityEngine;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;

//[RequireComponent(typeof(Player_Movement))]


public class Player_Graphics : Player_Controller
{

    private Transform m_Transform;
    private SpriteRenderer m_Sprite_Renderer;
    public Animator m_Animator;
    private Player_Movement m_Player_Movement;
    private Player_ID m_Player_ID;
    private float m_Impulse_Velocity;


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Sprite_Renderer = m_Transform.GetComponentInChildren<SpriteRenderer>();
        m_Player_Movement = m_Transform.GetComponent<Player_Movement>();
        m_Animator = m_Transform.GetComponentInChildren<Animator>();
    }


    private void Start ()
    {
        m_Sprite_Renderer.flipX = false;
        m_Animator.runtimeAnimatorController = get_Animator_Controller(m_Player_ID);
    }


    private void Update()
    {
        if (m_Player_Movement.get_Grounded)
        {
            m_Sprite_Renderer.flipX = (m_Player_Movement.get_Player_Direction == Player_Direction.Player_Direction_Left);
        }
    }



    public override void Intitalize_Player(Game_Controller game_Controller, Player_ID player_ID)
    {
        m_Player_ID = player_ID;
    }


    public void On_Ground_Contact(float impact_Velocity)
    {
        m_Animator.SetBool("Continue", true);
    }


    public void On_Player_Jump(float impulse_Velcity)
    {
        Vector2 velocity = m_Transform.GetComponent<Rigidbody2D>().velocity;
        m_Impulse_Velocity = m_Transform.InverseTransformDirection(velocity).y;
        m_Animator.SetBool("Jump", true);
    }

    public Player_ID Get_Player_ID
    {
        get { return m_Player_ID; }
    }


    private RuntimeAnimatorController get_Animator_Controller(Player_ID player_ID)
    {
        RuntimeAnimatorController anim_Controller = new RuntimeAnimatorController();

        switch (player_ID)
        {
            case Player_ID.Player_ID_One:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Character_Animator_Green",
                    typeof(RuntimeAnimatorController)
                );
                break;
            case Player_ID.Player_ID_Two:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Character_Animator_Red",
                    typeof(RuntimeAnimatorController)
                );
                break;
            case Player_ID.Player_ID_Three:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Character_Animator_Blue",
                    typeof(RuntimeAnimatorController)
                );
                break;
            case Player_ID.Player_ID_Four:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Character_Animator_Orange",
                    typeof(RuntimeAnimatorController)
                );
                break;
        }

        return anim_Controller;
    }

}
