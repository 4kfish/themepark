﻿using UnityEngine;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(PolygonCollider2D))]


public class Player_Movement : Player_Controller
{
    [Serializable]
    public class Movement_Physics
    {
        public float m_Movement_Speed;
        public float m_Acceleration;
        public float m_Jump_Height;
        [Range(0.01f, 0.1f)]
        public float m_Physics_Detection_Offset;
        public LayerMask m_Ground_Layer_Mask;
    }

    [Serializable]
    public class Movement_Effectors
    {
        [Range(0.1f, 0.9f)]
        public float m_Spike_Trap_Pickup;
        [Range(0.1f, 0.9f)]
        public float m_Needle_Trap_Pickup;
        [Range(0.1f, 0.9f)]
        public float m_Gum_Trap_Pickup;
    }

    public Movement_Physics m_Movement_Physics = new Movement_Physics();
    public Movement_Effectors m_Movement_Effectors = new Movement_Effectors();

    public float get_Horizontal_Axis_Raw { get { return get_Horizontal_Axis(m_Player_ID, true); } }
    public float get_Vertical_Axis_Raw { get { return get_Vertical_Axis(m_Player_ID, true); } }
    public bool get_Grounded { get { return m_Is_Grounded; } }
    public bool get_Jumping { get { return m_Jumping[1]; } }
    public Player_Direction get_Player_Direction { get { return m_Player_Direction; } }
    public AnimationCurve get_Acceleration_Curve { get { return m_Acceleration_Curve; } }
    public bool Grounded_On_Track { get; set; }
    public GameObject m_Smoke;

    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody;
    private CircleCollider2D m_Circle_Collider;
    private PolygonCollider2D[] m_Body_Colliders;
    private Game_Controller m_Game_Controller;

    public bool m_Is_Grounded, m_Accelerating;
    private bool[] m_Jumping;
    public AnimationCurve m_Acceleration_Curve = new AnimationCurve();
    private Player_ID m_Player_ID;
    private Player_Direction m_Player_Direction;
    public float m_Current_Speed;
    private float m_Relative_Mass;
    private Trap_Type m_Hold_Trap_Type;
    private float m_timer_Jump;
    private bool m_Jumped_anim;
    private GameObject explosion;

    public float Track_Torque { get { return m_Game_Controller.Track_Torque; } }
    private Vector2 Track_Center_Point { get { return m_Game_Controller.Track_Center_Point; } }


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Rigidbody = m_Transform.GetComponent<Rigidbody2D>();
        m_Circle_Collider = m_Transform.GetComponent<CircleCollider2D>();
        m_Body_Colliders = m_Transform.GetComponents<PolygonCollider2D>();
    }

	
	private void Start ()
    {
        m_Rigidbody.freezeRotation = true;
        m_Rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
        m_Rigidbody.WakeUp();

        Keyframe[] key_Points = new Keyframe[2];
        key_Points[0] = new Keyframe(0.0f, 0.0f);
        key_Points[0].outTangent = 12.0f;
        key_Points[1] = new Keyframe(1.0f, m_Movement_Physics.m_Movement_Speed);

        m_Acceleration_Curve = new AnimationCurve(key_Points);

        m_Player_Direction = Player_Direction.Player_Direction_Right;
        m_Body_Colliders[0].enabled = true;
        m_Body_Colliders[1].enabled = false;

        float arm_Mass = m_Transform.GetComponentInChildren<Player_Ragdoll_Physics>().Arm_Total_Mass;
        m_Relative_Mass = arm_Mass / m_Rigidbody.mass;

        m_Jumping = new bool[2];
    }
	
	
	private void Update ()
    {
        if (m_Transform.position.y < -16.0f)
        {
            m_Transform.position = Vector2.zero;
        }

        float axis_Value = get_Horizontal_Axis(m_Player_ID, true);

        if (!Mathf.Approximately(axis_Value, 0.0f))
        {
            
            Player_Direction player_Direction = (axis_Value < 0.0f) ? Player_Direction.Player_Direction_Left :
                Player_Direction.Player_Direction_Right;

            if (player_Direction != m_Player_Direction)
            {
                m_Player_Direction = player_Direction;
                m_Body_Colliders[0].enabled = (player_Direction == Player_Direction.Player_Direction_Right);
                m_Body_Colliders[1].enabled = (player_Direction == Player_Direction.Player_Direction_Left);
                m_Transform.GetComponentInChildren<Player_Ragdoll_Physics>().Flip_Ragdoll_Origin(m_Player_Direction);
            }

            if (!m_Accelerating && !Mathf.Approximately(m_Current_Speed, m_Movement_Physics.m_Movement_Speed)) {
                StartCoroutine(Acceleration());
            }
        }
        else
        {
            m_Current_Speed = 0.0f;
            m_Accelerating = false;
        }

        if (m_Is_Grounded && get_Jump_Button(m_Player_ID, Button_State.Button_State_Down))
        //if (m_Is_Grounded && Input.GetKeyDown(KeyCode.Space))
        {
            m_Jumping[0] = true;
        }

        m_Transform.rotation = Math_Ex.get_Look_Rotation(m_Transform.position, Track_Center_Point);
    }

    private void FixedUpdate ()
    {
        Ground_Physic_Cast(m_Is_Grounded);

        if (m_Jumping[0])
        {
            float force = Math_Ex.get_Jump_Force_Translation(m_Movement_Physics.m_Jump_Height, m_Rigidbody.gravityScale);
            force *= m_Relative_Mass;
            Vector2 direction = (Math_Ex.In_Value_Range(m_Transform.rotation.z, 0.0f, 180.0f)) ? Vector2.up : (Vector2)m_Transform.up;
            m_Rigidbody.AddForce(direction * force, ForceMode2D.Impulse);
            m_Jumping[0] = false;
            m_Jumping[1] = true;
            m_Transform.BroadcastMessage("On_Player_Jump", force, SendMessageOptions.RequireReceiver);
            m_Jumped_anim = true;

            //Player_Jump_Smoke;
            explosion = Instantiate(m_Smoke) as GameObject;
            explosion.GetComponent<Transform>().position = new Vector2(m_Rigidbody.position.x + 0.1f, GetComponentInChildren<SpriteRenderer>().bounds.min.y + 0.25f);
            explosion.transform.rotation = transform.rotation;

        }
        else if (m_Is_Grounded)
        {
            m_Rigidbody.velocity = Movement_Velocity(m_Player_Direction, m_Rigidbody.velocity);
            m_Rigidbody.AddForce(Centrifugal_Force(Track_Torque, m_Transform.position, Track_Center_Point), ForceMode2D.Force);
        }

        if (m_Jumped_anim)
        {
            m_timer_Jump += Time.deltaTime;
            if (m_timer_Jump > 1.23f)
            {
                Destroy(explosion);
                m_Jumped_anim = false;
                m_timer_Jump = 0;
            }
        }
    }


    private void OnTriggerEnter2D (Collider2D collider)
    {

    }


    public override void Intitalize_Player (Game_Controller game_Controller, Player_ID player_ID)
    {
        m_Game_Controller = game_Controller;
        m_Player_ID = player_ID;
    }


    public void On_Trap_Pickup (Trap_Type trap_Type)
    {
        m_Hold_Trap_Type = trap_Type;
    }


    public void On_Trap_Droped ()
    {
        m_Hold_Trap_Type = Trap_Type.Trap_Type_None;
    }


    private void Ground_Physic_Cast (bool prev_Grounded)
    {
        float radius = m_Circle_Collider.radius;
        float distance = radius + m_Movement_Physics.m_Physics_Detection_Offset;
        Vector2 origin = m_Transform.position + (m_Transform.up * radius);
        Vector2 direction = -m_Transform.up;

        RaycastHit2D hit_Info = Physics2D.CircleCast(origin, radius, -m_Transform.up, distance, m_Movement_Physics.m_Ground_Layer_Mask);
        m_Is_Grounded = (hit_Info.collider != null);

        if (hit_Info.collider != null && hit_Info.collider.transform == m_Game_Controller.m_Track_Transform)
        {
            Grounded_On_Track = true;
        }
        else
        {
            Grounded_On_Track = false;
        }


        if (m_Is_Grounded && !prev_Grounded)
        {
            if (m_Jumping[1])
            {
                Vector2 local_Velocity = m_Transform.InverseTransformDirection(m_Rigidbody.velocity);
                m_Transform.BroadcastMessage("On_Ground_Contact", Mathf.Abs(local_Velocity.y), SendMessageOptions.RequireReceiver);
            }
            m_Jumping[1] = false;
        }
    }


    private Vector2 Movement_Velocity(Player_Direction player_Direction, Vector2 velocity)
    {
        var current_Velocity_Y = m_Transform.InverseTransformDirection(velocity).y;
        var direction = Vector2.right * (int)player_Direction;
        direction.x *= Ground_Speed_Effect(m_Current_Speed, m_Hold_Trap_Type);
        direction.y = current_Velocity_Y;
        return m_Transform.TransformDirection(direction);
    }


    private Vector2 Centrifugal_Force (float torque, Vector2 position, Vector2 center_Point)
    {
        var normal = (position - center_Point).normalized;
        return normal * torque;
    }


    private IEnumerator Acceleration()
    {
        float acceleration_Value = 0.0f;
        m_Accelerating = true;

        while (acceleration_Value < 1.0f && m_Accelerating)
        {
            acceleration_Value = Math_Ex.Const_Lerp(acceleration_Value, 1.0f, m_Movement_Physics.m_Acceleration, Time.deltaTime);
            m_Current_Speed = m_Acceleration_Curve.Evaluate(acceleration_Value);

            if (Mathf.Approximately(acceleration_Value, 1.0f) || !m_Accelerating)
            {
                m_Current_Speed = m_Movement_Physics.m_Movement_Speed;
                break;
            }
            yield return null;
        }

        m_Accelerating = false;
    }


    private float Ground_Speed_Effect(float original_Speed, Trap_Type trap_Type)
    {
        float effect_Percent = 1.0f;

        switch(trap_Type)
        {
            case Trap_Type.Trap_Type_Gum: effect_Percent = m_Movement_Effectors.m_Gum_Trap_Pickup;
                break;
            case Trap_Type.Trap_Type_Needle: effect_Percent = m_Movement_Effectors.m_Needle_Trap_Pickup;
                break;
            case Trap_Type.Trap_Type_Spikes: effect_Percent = m_Movement_Effectors.m_Spike_Trap_Pickup;
                break;
        }

        return original_Speed * effect_Percent;
    }

}
