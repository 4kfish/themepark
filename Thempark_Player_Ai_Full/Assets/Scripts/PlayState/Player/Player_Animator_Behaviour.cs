﻿using UnityEngine;
using System.Collections;


public class Player_Animator_Behaviour : StateMachineBehaviour
{

    private Player_Movement m_Player_Movement;
   

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_Player_Movement == null)
        {
            m_Player_Movement = animator.transform.root.GetComponent<Player_Movement>();
        }

        if (stateInfo.IsName("Jump"))
        {
            animator.SetBool("Jump", false);
        }
        else if (stateInfo.IsName("Sprint"))
        {
            animator.SetBool("Continue", false);
        }
        else if (stateInfo.IsName("Idle"))
        {
            animator.SetBool("Continue", false);
        }
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Mathf.Abs(m_Player_Movement.get_Horizontal_Axis_Raw) > 0.0f)
        {
            animator.SetFloat("Axis_Input", 1.0f);
        }
        else if (Mathf.Approximately(m_Player_Movement.get_Horizontal_Axis_Raw, 0.0f))
        {
            animator.SetFloat("Axis_Input", -1.0f);
        }
        if (stateInfo.IsName("Jump") && m_Player_Movement.Grounded_On_Track)
        {
            animator.SetBool("Continue", true);
        }
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
	
    }


}



