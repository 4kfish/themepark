﻿using UnityEngine;
using System.Collections;
using System.Collections.Specialized;
using Player_Data_Collection;


public class Player_Ragdoll_Physics : MonoBehaviour
{

    public Vector2 m_Arm_Offset_Left;
    public Vector2 m_Arm_Offset_Right;
    public float m_Arm_Total_Mass;

    public float Arm_Total_Mass { get { return m_Arm_Total_Mass; } }

    private Transform m_Transform;
    private Transform[] m_Arm_Segments;
    public OrderedDictionary m_Arm_Physics_Components = new OrderedDictionary();


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();

        HingeJoint2D[] joints = m_Transform.GetComponentsInChildren<HingeJoint2D>();
        m_Arm_Segments = new Transform[joints.Length];

        for (int i = 0; i < joints.Length; i++)
        {
            Rigidbody2D rigidbody = joints[i].GetComponent<Rigidbody2D>();
            m_Arm_Segments[i] = joints[i].GetComponent<Transform>();
            m_Arm_Physics_Components.Add(rigidbody, joints[i]);
        }
    }


	void Start ()
    {
        HingeJoint2D joint = (HingeJoint2D)m_Arm_Physics_Components[0];
        joint.connectedAnchor = m_Arm_Offset_Right;
	}
	
	
	void Update ()
    {
	    
	}


    public void Flip_Ragdoll_Origin(Player_Direction player_Direction)
    {
        if (player_Direction == Player_Direction.Player_Direction_Right)
        {
            HingeJoint2D joint = (HingeJoint2D)m_Arm_Physics_Components[0];
            joint.connectedAnchor = m_Arm_Offset_Right;

            for (int i = 1; i < m_Arm_Segments.Length; i++)
            {
                Vector2 position = m_Arm_Segments[i].position;
                position.x += m_Arm_Offset_Right.x;
                m_Arm_Segments[i].position = position;
            }
        }
        else
        {
            HingeJoint2D joint = (HingeJoint2D)m_Arm_Physics_Components[0];
            joint.connectedAnchor = m_Arm_Offset_Left;
            for (int i = 1; i < m_Arm_Segments.Length; i++)
            {
                Vector2 position = m_Arm_Segments[i].position;
                position.x += m_Arm_Offset_Right.x;
                m_Arm_Segments[i].position = position;
            }
        }
    }
}
