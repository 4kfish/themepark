﻿using UnityEngine;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;

[AddComponentMenu("Scripts/Player/Player Controller")]


public class Player_Controller : MonoBehaviour
{

    protected enum Button_State
    {
        Button_State_Default,
        Button_State_Down,
        Button_State_Up
    }

    protected static float get_Vertical_Axis(Player_ID player_Id, bool raw)
    {
        float value = 0.0f;

        switch (player_Id)
        {
            case Player_ID.Player_ID_One:
                value = (raw) ? Input.GetAxisRaw("P1_Vertical") : Input.GetAxis("P1_Vertical");
                break;
            case Player_ID.Player_ID_Two:
                value = (raw) ? Input.GetAxisRaw("P2_Vertical") : Input.GetAxis("P2_Vertical");
                break;
            case Player_ID.Player_ID_Three:
                value = (raw) ? Input.GetAxisRaw("P3_Vertical") : Input.GetAxis("P3_Vertical");
                break;
            case Player_ID.Player_ID_Four:
                value = (raw) ? Input.GetAxisRaw("P4_Vertical") : Input.GetAxis("P4_Vertical");
                break;

        }
        return value;
    }

    protected static float get_Horizontal_Axis(Player_ID player_Id, bool raw)
    {
        float value = 0.0f;

        switch (player_Id)
        {
            case Player_ID.Player_ID_One:
                value = (raw) ? Input.GetAxisRaw("P1_Horizontal") : Input.GetAxis("P1_Horizontal");
                break;
            case Player_ID.Player_ID_Two:
                value = (raw) ? Input.GetAxisRaw("P2_Horizontal") : Input.GetAxis("P2_Horizontal");
                break;
            case Player_ID.Player_ID_Three:
                value = (raw) ? Input.GetAxisRaw("P3_Horizontal") : Input.GetAxis("P3_Horizontal");
                break;
            case Player_ID.Player_ID_Four:
                value = (raw) ? Input.GetAxisRaw("P4_Horizontal") : Input.GetAxis("P4_Horizontal");
                break;

        }
        return value;
    }

    protected static bool get_Jump_Button(Player_ID player_Id, Button_State button_State)
    {
        bool value = false;

        switch (player_Id)
        {
            case Player_ID.Player_ID_One:
                value = get_Button_State(button_State, "P1_Jump");
                break;
            case Player_ID.Player_ID_Two:
                value = get_Button_State(button_State, "P2_Jump");
                break;
            case Player_ID.Player_ID_Three:
                value = get_Button_State(button_State, "P3_Jump");
                break;
            case Player_ID.Player_ID_Four:
                value = get_Button_State(button_State, "P4_Jump");
                break;

        }
        return value;
    }

    protected static bool get_Interact_Button(Player_ID player_Id, Button_State button_State)
    {
        bool value = false;

        switch (player_Id)
        {
            case Player_ID.Player_ID_One:
                value = get_Button_State(button_State, "P1_Interact");
                break;
            case Player_ID.Player_ID_Two:
                value = get_Button_State(button_State, "P2_Interact");
                break;
            case Player_ID.Player_ID_Three:
                value = get_Button_State(button_State, "P3_Interact");
                break;
            case Player_ID.Player_ID_Four:
                value = get_Button_State(button_State, "P4_Interact");
                break;

        }
        return value;
    }

    protected static bool get_Left_Bumper_Button(Player_ID player_Id, Button_State button_State)
    {
        bool value = false;

        switch (player_Id)
        {
            case Player_ID.Player_ID_One:
                value = get_Button_State(button_State, "P1_Left_Bumper");
                break;
            case Player_ID.Player_ID_Two:
                value = get_Button_State(button_State, "P2_Left_Bumper");
                break;
            case Player_ID.Player_ID_Three:
                value = get_Button_State(button_State, "P3_Left_Bumper");
                break;
            case Player_ID.Player_ID_Four:
                value = get_Button_State(button_State, "P4_Left_Bumper");
                break;

        }
        return value;
    }

    protected static bool get_Right_Bumper_Button(Player_ID player_Id, Button_State button_State)
    {
        bool value = false;

        switch (player_Id)
        {
            case Player_ID.Player_ID_One:
                value = get_Button_State(button_State, "P1_Right_Bumper");
                break;
            case Player_ID.Player_ID_Two:
                value = get_Button_State(button_State, "P2_Right_Bumper");
                break;
            case Player_ID.Player_ID_Three:
                value = get_Button_State(button_State, "P3_Right_Bumper");
                break;
            case Player_ID.Player_ID_Four:
                value = get_Button_State(button_State, "P4_Right_Bumper");
                break;

        }
        return value;
    }

    private static bool get_Button_State(Button_State button_State, string button_Id)
    {
        bool value = false;

        switch (button_State)
        {
            case Button_State.Button_State_Default:
                value = Input.GetButton(button_Id);
                break;
            case Button_State.Button_State_Down:
                value = Input.GetButtonDown(button_Id);
                break;
            case Button_State.Button_State_Up:
                value = Input.GetButtonUp(button_Id);
                break;
        }

        return value;
    }


    public virtual void Intitalize_Player(Game_Controller game_Controller, Player_ID player_ID)
    {

    }


}

