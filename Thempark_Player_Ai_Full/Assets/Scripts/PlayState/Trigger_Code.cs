﻿using UnityEngine;
using System.Collections;

public class Trigger_Code : MonoBehaviour {
    private GameObject m_PointCode;
    private PointCheck m_Point_Check_Code;

	// Use this for initialization
	void Start ()
    {
        m_PointCode = GameObject.Find("PointCode");
        m_Point_Check_Code = m_PointCode.GetComponent<PointCheck>();

    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            m_Point_Check_Code.m_In_Waterfall = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            m_Point_Check_Code.m_In_Waterfall = false;
        }
    }
}
