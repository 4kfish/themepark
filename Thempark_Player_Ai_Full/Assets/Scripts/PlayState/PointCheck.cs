﻿using UnityEngine;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PointCheck : MonoBehaviour
{
    public Transform[] m_Players;           //The list with all the players in.
    public Transform[] GetPlayerlist { get { return m_Players_Copy; } }
    public GameObject m_Goal_Point;         //The point where the players must be closest to.
    public Transform[] m_Player_Win_Anim;   //All the animations for the winner.
    public Transform[] m_Points_List;       //The list of all the HUD Fillers. Can be merged with the m_Player_HUDs.
    public Transform[] m_Player_HUDs;       //The list of all the HUDs.
    public float[] m_Timer_List;            //The list with all the players score.
    public int player_Amount;


    //NO
    private Transform[] m_Points_List_Copy;
    private Transform[] m_Players_Copy;
    //

    private GameObject sceneGameObject;
    private GameObject m_Player_Points;
    private GameObject m_Win_Animation;
    private GameObject m_Object_Spawner;
    private GameObject m_Coin_Spawner;
    private GameObject m_Crown;
    private GameObject m_Game_Controller;
    private Game_Controller m_Game_Controller_Script;
    private SpriteRenderer m_Crown_Renderer;
    private Box_Prefab_Timer m_Object_Spawner_Script;
    private Coin_Spawner m_Coin_Spawner_Script;
    private Animator m_Animator_Controller;
    private float m_PTimer = 0.4285714285f;
    private float m_Change_Scene_Timer;
    private int index = 0;
    public bool m_In_Waterfall = false;

    public bool m_Change_Scene_Bool = false;

    void Start()
    {
        m_Game_Controller = GameObject.Find("Scene");
        m_Game_Controller_Script = m_Game_Controller.GetComponent<Game_Controller>();
        player_Amount = (int)m_Game_Controller_Script.m_Player_Amount;

        m_Timer_List = new float[player_Amount];
        m_Points_List = new Transform[player_Amount];
        m_Player_HUDs = new Transform[player_Amount];
        m_Player_Win_Anim = new Transform[player_Amount];
        m_Animator_Controller = GetComponent<Animator>();

        sceneGameObject = GameObject.Find("Scene");
        m_Players = sceneGameObject.GetComponent<Game_Controller>().players;

        m_Goal_Point = GameObject.Find("Goal Point");

        for (int x = 0; x < m_Timer_List.Length; x++)
        {
            m_Timer_List[x] = m_PTimer;

        }
        for (int x = 0; x < m_Points_List.Length; x++)
        {
            int i = x + 1;
            m_Player_Points = GameObject.Find("HUD_" + i.ToString());
            m_Points_List[x] = m_Player_Points.transform;

            m_Player_HUDs[x] = GameObject.Find("Player " + i.ToString() + " HUD").transform;
        }

        m_Object_Spawner = GameObject.Find("Object Spawn");
        m_Object_Spawner_Script = m_Object_Spawner.GetComponent<Box_Prefab_Timer>();

        m_Coin_Spawner = GameObject.Find("Coin Spawn");
        m_Coin_Spawner_Script = m_Coin_Spawner.GetComponent<Coin_Spawner>();

        m_Crown = GameObject.Find("Krona 1st");
        m_Crown_Renderer = GetComponentInChildren<SpriteRenderer>();
        m_Crown.SetActive(false);
    }

    void FixedUpdate()
    {
        bool HUD_enlargement = false;
        if (m_In_Waterfall)
        {
            m_Timer_List[index] -= Time.fixedDeltaTime;
            HUD_enlargement = true;
        }

        if(HUD_enlargement)
        {
            m_Player_HUDs[index].GetComponent<RectTransform>().localScale = new Vector3(1.3f, 1.3f);
            HUD_enlargement = false;
        }
        else if(!HUD_enlargement)
        {
            m_Player_HUDs[index].GetComponent<RectTransform>().localScale = new Vector3(1, 1);
        }

        if (m_Points_List[index].GetComponent<Image>().fillAmount >= 1)
        {
            Player_Won();
        }
        else
        {
            float min_Distance = Vector2.Distance(m_Players[0].transform.position, m_Goal_Point.transform.position);
            index = 0;
            for (int x = 1; x < m_Players.Length; x++)
            {
                float distance = Vector2.Distance(m_Players[x].transform.position, m_Goal_Point.transform.position);
                if (distance < min_Distance)
                {
                    min_Distance = distance;
                    index = x;
                }
            }
        }
        if (m_Points_List[index].GetComponent<Image>().fillAmount <= 1 && m_In_Waterfall)
        {
            m_Points_List[index].GetComponent<Image>().fillAmount += 0.02857142857f * Time.deltaTime;
            HUD_enlargement = true;
        }

        if (m_Change_Scene_Bool)
        {
            m_Change_Scene_Timer += Time.deltaTime;
            if (m_Change_Scene_Timer >= 7.5)
                SceneManager.LoadScene("Victory_Scene");
        }
    }

    public void Player_Won()
    {
        m_Change_Scene_Bool = true;
        m_Animator_Controller.SetBool("Finished", true);
        m_Animator_Controller.SetInteger("Winning_Player", index);

        sceneGameObject.GetComponent<Game_Controller>().m_Track_Rotation_Speed = 0;
        m_Object_Spawner_Script.enabled = false;
        m_Coin_Spawner_Script.enabled = false;

        m_Points_List_Copy = m_Points_List;
        m_Players_Copy = m_Players;
        //sort(m_Points_List_Copy, m_Players_Copy);

        float[] pointsFloatList;
        pointsFloatList = new float[player_Amount];
        for(int x = 0; x < m_Points_List_Copy.Length; x++)
        {
            pointsFloatList[x] = m_Points_List_Copy[x].GetComponent<Image>().fillAmount;
        }

        Array.Sort(pointsFloatList, m_Players_Copy);

        //Array.Reverse(m_Timer_List);
        //Array.Reverse(m_Players_Copy);
    }

    void sort(Transform[] data, Transform[] m_Players)
    {
        float tmp = 0;

        for (int i = 0; i < data.Length; i++)
        {
            for (int j = 0; j < data.Length - 1; j++)
            {
                if (data[j].GetComponent<Image>().fillAmount > data[j + 1].GetComponent<Image>().fillAmount)
                {
                    tmp = data[j + 1].GetComponent<Image>().fillAmount;
                    data[j + 1].GetComponent<Image>().fillAmount = data[j].GetComponent<Image>().fillAmount;
                    data[j].GetComponent<Image>().fillAmount = tmp;
                    Transform Trans_Temp = m_Players[j + 1];
                    m_Players[j + 1] = m_Players[j];
                    m_Players[j] = Trans_Temp;
                }
            }
        }
    }

    //void OnTriggerStay2D(Collider2D col)
    //{
    //    Debug.Log("lol2");
    //    if(col.gameObject.CompareTag("Waterfall"))
    //    {
    //        m_In_Waterfall = true;
    //    }
    //}
}

