﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using Player_Data_Collection;
using Utility_Data_Collection;

public class Timer_Script : MonoBehaviour {

    public float m_Time_Left;
    public Text m_text;
    public int m_Number_Of_Events;
    public int[] m_Event;

    private float m_Time_Original;

	// Use this for initialization
	void Start ()
    {
        m_Event = new int[m_Number_Of_Events];
        m_Event[0] = 1;
        m_Event[1] = 2;
        m_Time_Original = m_Time_Left;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        m_Time_Left -= Time.deltaTime;
        m_text.text = Mathf.Round(m_Time_Left).ToString();

        if(m_Time_Left < 0)
        {
            m_Time_Left = m_Time_Original;
            int tmp = m_Event[0];
            m_Event[0] = m_Event[1];
            m_Event[1] = tmp;
        }
	}
}
