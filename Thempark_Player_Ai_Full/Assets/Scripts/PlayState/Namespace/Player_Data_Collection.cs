﻿using UnityEngine;
using System.Collections;


namespace Player_Data_Collection
{

    public enum Player_Amount
    {
        Player_Amount_One = 1,
        Player_Amount_Two = 2,
        Player_Amount_Three = 3,
        Player_Amount_Four = 4
    }

    public enum Player_Direction
    {
        Player_Direction_Left = -1,
        Player_Direction_Right = 1
    }

    public enum Jump_Direction
    {
        Jump_Direction_Left,
        Jump_Direction_Right,
        Jump_Direction_Up
    }

    public enum Player_ID
    {
        Player_ID_One,
        Player_ID_Two,
        Player_ID_Three,
        Player_ID_Four
    }

}
