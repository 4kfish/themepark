﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;


namespace Utility_Data_Collection
{

    public enum Trap_Type
    {
        Trap_Type_None,
        Trap_Type_Needle,
        Trap_Type_Gum,
        Trap_Type_Spikes
    }

    public static class Math_Ex
    {
        public static Vector2 Angled_Unit_Vector(float angle)
        {
            var angle_X = Mathf.Sin(angle * Mathf.Deg2Rad);
            var angle_Y = Mathf.Cos(angle * Mathf.Deg2Rad);
            return new Vector2(angle_X, angle_Y);
        }

        public static Quaternion Clamp_Rotation_Axis(Quaternion rotation, float min, float max)
        {
            rotation.x /= rotation.w;
            rotation.y /= rotation.w;
            rotation.z /= rotation.w;
            rotation.w = 1.0f;

            float angle_Z = 2.0f * Mathf.Rad2Deg * Mathf.Atan(rotation.z);

            angle_Z = Mathf.Clamp(angle_Z, min, max);

            rotation.z = Mathf.Tan(0.5f * Mathf.Deg2Rad * angle_Z);

            return rotation;
        }

        public static Quaternion get_Look_Rotation(Vector2 position, Vector2 look_Point)
        {
            Vector2 direction = (look_Point - position).normalized;
            float rotation_Z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            return Quaternion.Euler(0f, 0f, rotation_Z - 90);
        }

        public static float get_Jump_Force_Translation(float jump_Height, float gravity_Scale)
        {
            return Mathf.Sqrt(2f * jump_Height * Mathf.Abs(Physics2D.gravity.y * gravity_Scale));
        }

        public static float Const_Lerp(float from, float to, float speed, float delta_Time)
        {
            float distance = Mathf.Abs(from - to);
            return Mathf.Lerp(from, to, (speed / distance) * delta_Time);
        }

        public static Vector2 Const_Vctr_Lerp(Vector2 from, Vector2 to, float speed, float delta_Time)
        {
            float distance = Vector2.Distance(from, to);
            return Vector2.Lerp(from, to, (speed / distance) * delta_Time);
        }

        public static float Round_Float(float value, int decimals)
        {
            return (float)Math.Round((double)value, 2);
        }

        public static Vector2 Round_Vctr(Vector2 vector, int decimals)
        {
            return new Vector2
                (
                (float)Math.Round((double)vector.x, 2),
                (float)Math.Round((double)vector.y, 2)
                );
        }


        public static bool In_Value_Range(float value, float min, float max)
        {
            return (value > min && value < max);
        }

        public static bool In_Value_Range(int value, int min, int max)
        {
            return (value > min && value < max);
        }


    }
}
