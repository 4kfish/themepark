﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpringJoint2D), typeof(LineRenderer))]
public class Rubber_Gum : MonoBehaviour
{
    List<Vector3> linePoints = new List<Vector3>();
    LineRenderer lineRenderer;
    SpringJoint2D joint;
    PolygonCollider2D m_Poly_Collider;
    Vector2 direction;
    public Rigidbody2D m_Target_Rigidbody;
    Transform m_gum_Line;
    public float threshold = 0.001f;
    public float m_Distance = 1f;
    public float m_break_Distance;
    public bool m_Grabbed;
    public bool m_is_broken = true;
    float distance = 0.0f;

    //4Head
    public Transform BubbleGum_On_Target;
    public bool m_is_Attached = false;


    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        joint = GetComponent<SpringJoint2D>();
        m_Poly_Collider = GetComponent<PolygonCollider2D>();
        lineRenderer.useWorldSpace = true;

    }
	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (m_Target_Rigidbody != null)
            distance = Vector2.Distance(transform.position, m_Target_Rigidbody.position);
        if (distance > m_break_Distance && m_is_Attached)
        {
            joint.enabled = false;
            lineRenderer.enabled = false;
            joint.connectedBody = null;
            m_is_broken = true;
            if(BubbleGum_On_Target != null)
                Destroy(BubbleGum_On_Target.gameObject);
            BubbleGum_On_Target = null;
            m_Target_Rigidbody = null;
            m_is_Attached = false;
        }
        if(m_gum_Line != null && !m_is_broken)
        {
            lineRenderer.enabled = true;
            lineRenderer.SetVertexCount(2);
            m_gum_Line.position = m_Target_Rigidbody.position;
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, m_gum_Line.position);
            BubbleGum_On_Target.position = m_Target_Rigidbody.transform.position;
            Vector3 moveDirection = gameObject.transform.position - BubbleGum_On_Target.position;
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            BubbleGum_On_Target.rotation = Quaternion.AngleAxis(angle - 180f, Vector3.forward);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col != null)
        {
            if (!col.gameObject.CompareTag("Arm_Segment"))
            {
                m_Target_Rigidbody = col.transform.GetComponent<Rigidbody2D>();
            }
            if (!m_is_Attached)
            {
                m_is_broken = false;
                m_gum_Line = col.transform;
                joint.enabled = true;
                joint.connectedBody = m_Target_Rigidbody;
                joint.distance = Vector2.Distance(transform.position, col.transform.position);
                direction = (col.transform.position - transform.position).normalized;
                BubbleGum_On_Target = Instantiate(Resources.Load("Gum On Player", typeof(Transform))) as Transform;
                m_is_Attached = true;
            }
        }
    }
}
