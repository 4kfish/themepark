﻿using UnityEngine;
using System.Collections;
using Player_Data_Collection;
using System;
using Utility_Data_Collection;
using UnityEngine.UI;


public class CoinCollectionScript : Player_Graphics
{
    public bool m_hit = false;
    private SpriteRenderer m_Sprite_Renderer;
    private float m_Timer;
    private PointCheck m_Point_Check;
    private Player_ID m_PlayerID;

    IEnumerator FlashCharacter()
    {
        m_Sprite_Renderer.color = Color.yellow;
        yield return new WaitForSeconds(0.1f);
        m_Sprite_Renderer.color = Color.white;
        m_hit = false;
        Destroy(gameObject);
    }

    // Use this for initialization
    void Start ()
    {
        GameObject PointCodeRef = GameObject.Find("PointCode");
        m_Point_Check = PointCodeRef.GetComponent<PointCheck>();

	}
	
	// Update is called once per frame
	void Update ()
    {
        if(m_hit)
        {
            StartCoroutine(FlashCharacter());
            SpriteRenderer m_Spriterenderer = GetComponent<SpriteRenderer>();
            m_Spriterenderer.enabled = false;

            m_Point_Check.m_Points_List[(int)m_PlayerID].GetComponent<Image>().fillAmount += 0.002857142857f / 2;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            //Det här är till för att veta vilken karaktär som träffas av myntet. Ändra om detta så det stämmer med den nya koden.
            m_PlayerID = col.gameObject.GetComponent<Player_Graphics>().Get_Player_ID;
            m_Sprite_Renderer = col.gameObject.GetComponentInChildren<SpriteRenderer>();
            m_hit = true;
        }
    }
}
