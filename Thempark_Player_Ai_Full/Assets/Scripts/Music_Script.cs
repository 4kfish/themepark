﻿using UnityEngine;
using System.Collections;

public class Music_Script : MonoBehaviour
{
    public AudioSource m_Source;
    public AudioClip m_Clip_One;
    public AudioClip m_Clip_Two;
    private bool m_Played = false;
	
	// Update is called once per frame
	void Update ()
    {
        if(!m_Played)
        {
            m_Source.PlayOneShot(m_Clip_One);
            m_Played = true;
        }
        else if(!m_Source.isPlaying)
        {
            m_Source.PlayOneShot(m_Clip_Two);
        }

	}
}
