﻿using UnityEngine;
using System.Collections;

//Seperate for arrow and needletrap.

public class NeedleImpalementScript : MonoBehaviour
{
    private PolygonCollider2D m_Needle_Collider;
    private Rigidbody2D m_Needle_Rigidbody;
    private Rigidbody2D m_Target_Rigidbody;
    private bool[] m_Launch_Proj;
    public float m_Force = 2f;
    

    // Use this for initialization
    void Start()
    {
        m_Needle_Collider = GetComponent<PolygonCollider2D>();
        m_Needle_Rigidbody = GetComponent<Rigidbody2D>();
        m_Needle_Rigidbody.isKinematic = true;
        m_Launch_Proj = new bool[2];
    }

    // Update is called once per frame
    void Update()
    {
        //Temp activation Code
        if (Input.GetKeyDown(KeyCode.G) && !m_Launch_Proj[1])
        {
            m_Launch_Proj[1] = true;
            m_Launch_Proj[0] = true;
        }
        //
    }
    void FixedUpdate()
    {
        if(m_Launch_Proj[0])
        {
            m_Needle_Rigidbody.isKinematic = false;
            Vector2 direction = transform.up;
            m_Needle_Rigidbody.AddForce(direction * m_Force, ForceMode2D.Impulse);
            m_Launch_Proj[0] = false;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //When the colliders hit the target the rotation i z is unlocked and makes the player "limp".
        //It also makes the spike colliders not hit the colliders on the targets body and turns the hit timer boolean to true.
        if (collision.collider.tag != "Arm_Segment" || collision.collider.tag != "Main_Track")
        {
            //Alternative to add a death boolean or such to kill the character on impact.
            m_Target_Rigidbody = collision.transform.GetComponent<Rigidbody2D>();
            m_Needle_Rigidbody.velocity = Vector2.zero;
            m_Needle_Rigidbody.isKinematic = true;
            m_Needle_Rigidbody.transform.parent = m_Target_Rigidbody.transform;
            m_Launch_Proj[1] = false;
            m_Needle_Collider.enabled = false;
            
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {

    }
}
