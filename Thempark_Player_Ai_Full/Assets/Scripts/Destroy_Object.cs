﻿using UnityEngine;
using System.Collections;

public class Destroy_Object : MonoBehaviour
{
    public float m_Time_until_Destroy;
    private float m_Time_Elapsed;
    private GameObject explosion;
    private Rigidbody2D m_Rigidbody;
    private bool spawn = true;
    // Use this for initialization
    void Start ()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Destroy(gameObject, m_Time_until_Destroy);
        m_Time_Elapsed += Time.deltaTime;
        if(m_Time_Elapsed > m_Time_until_Destroy - 0.5)
            transform.localScale -= new Vector3(Time.deltaTime, Time.deltaTime);

        if (transform.localScale.x < 0.1f && transform.localScale.y < 0.1f)
        {
            transform.localScale = new Vector3(0.1f,0.1f);
        }

        if(m_Time_Elapsed > m_Time_until_Destroy && spawn)
        {
            explosion = Instantiate(Resources.Load("Object_Disappear_Smoke_Sheet_0", typeof(GameObject))) as GameObject;
            explosion.GetComponent<Transform>().position = new Vector2(m_Rigidbody.position.x + 0.1f, GetComponentInChildren<SpriteRenderer>().bounds.min.y + 0.25f);
            explosion.transform.rotation = transform.rotation;
            spawn = false;
        }
    }
}
