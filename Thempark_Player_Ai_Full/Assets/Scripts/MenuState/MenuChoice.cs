﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuChoice : MonoBehaviour
{
    public bool get_starting { get { return m_isStarting; } }
    public bool get_startRun { get { return m_StartRun; } }
    public float get_playerNr { get { return m_NrPlayers; } }

    public bool m_isStarting, m_StartRun;
    public bool[] m_isActive;

    public int m_NrPlayers;
    public GameObject m_Sock1;
    public GameObject m_Sock2;
    public GameObject m_Sock3;
    public GameObject m_Sock4;

    public Transform m_Fade;

    private float m_Delay;
    
    private GameObject m_Sock1Clone;
    private GameObject m_Sock2Clone;
    private GameObject m_Sock3Clone;
    private GameObject m_Sock4Clone;

    private Transform m_FadeClone;

    private float m_alpha = 0;

    private bool m_Sock1_Exists = true, m_Sock2_Exists, m_Sock3_Exists, m_Sock4_Exists;
    
    void Start()
    {
        m_isActive = new bool[4];

        //Creates the first player
        m_NrPlayers++;
        m_Sock1Clone = Instantiate(m_Sock1, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
        m_Sock1Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
        m_Sock1Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
        m_isActive[0] = true;

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && !m_isActive[1])//Creates player two
        {
            m_NrPlayers++;
            m_Sock2Clone = Instantiate(m_Sock2, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
            m_Sock2Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
            m_Sock2Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
            m_isActive[1] = true;
        }
        if (Input.GetKeyDown(KeyCode.RightControl) && !m_isActive[2])//Creates player three
        {
            m_NrPlayers++;
            m_Sock3Clone = Instantiate(m_Sock3, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
            m_Sock3Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
            m_Sock3Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
            m_isActive[2] = true;
        }
        if (Input.GetKeyDown(KeyCode.B) && !m_isActive[3])//Creates player four
        {
            m_NrPlayers++;
            m_Sock4Clone = Instantiate(m_Sock4, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
            m_Sock4Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
            m_Sock4Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
            m_isActive[3] = true;
        }


        if (Input.GetKeyDown(KeyCode.E) && m_isActive[3])//Destroys player four
        {
            Destroy(m_Sock4Clone);
            m_isActive[3] = false;
            m_NrPlayers--;
        }
        if (Input.GetKeyDown(KeyCode.RightShift) && m_isActive[2])//Destroys player three
        {
            Destroy(m_Sock3Clone);
            m_isActive[2] = false;
            m_NrPlayers--;
        }
        if (Input.GetKeyDown(KeyCode.S) && m_isActive[1])//Destroys player two
        {
            Destroy(m_Sock2Clone);
            m_isActive[1] = false;
            m_NrPlayers--;
        }


        if (Input.GetKeyDown(KeyCode.LeftControl) && !m_isStarting && m_NrPlayers > 1)
        {
            m_isStarting = true;//Starts the transition to playstate
            transform.position = new Vector3(transform.position.x - 0.35f, transform.position.y, transform.position.z);//offset because the animation isn centered
            m_FadeClone = Instantiate(m_Fade, new Vector3(0, 0, 0), Quaternion.identity) as Transform;//Instantiates fade
        }
    }
    
    void FixedUpdate()
    {
        /*  if (Input.GetKeyDown(KeyCode.Q) && m_NrPlayers < 4 && !m_Sock1_Exists)
          {
                m_NrPlayers++;
                m_Sock1Clone = Instantiate(m_Sock1, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
                m_Sock1Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
                m_Sock1Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
                m_Sock1_Exists = true;
          }
          else*/
        

        if(m_isStarting)
        {
            m_Delay += Time.deltaTime;
        }

        if(m_Delay >= 1.2f)
        {
            m_StartRun = true;//Players start running towards the tumblr
        }

        if(m_Delay >= 3.8f)
        {
            float m_camSize = Camera.main.orthographicSize;//Camera zoom
            m_camSize -= 0.02f;//
            Camera.main.orthographicSize = m_camSize;//
            if (Camera.main.transform.position.y > -1.5f)//
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y - 0.015f, Camera.main.transform.position.z);//
        }
        if (m_Delay >= 3.9f)
        {
            m_alpha += 0.01f;//Fade out starts
            m_FadeClone.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, m_alpha);
        }
        if (m_Delay >= 5.9f)
        {
            SceneManager.LoadScene(1);//Next scene
        }
        /*
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(1);
        }*/
    }
}