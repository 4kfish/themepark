﻿using UnityEngine;
using System.Collections;

public class Tumbler_Jump_Behaviour : StateMachineBehaviour
{
    private MenuChoice m_menuChoice;
         
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_menuChoice == null)
        {
            m_menuChoice = animator.transform.root.GetComponent<MenuChoice>();
        }
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsTag("State_Idle") && !animator.GetBool("PlayStart"))
        {
            if (m_menuChoice.get_starting)
            {
                animator.SetBool("PlayStart", true);
            }
        }
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

}
