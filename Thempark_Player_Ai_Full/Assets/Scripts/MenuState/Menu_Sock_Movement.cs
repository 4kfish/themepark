﻿using UnityEngine;
using System.Collections;

public class Menu_Sock_Movement : MonoBehaviour {

    public bool get_moving { get { return m_isMoving; } }
    public bool get_jumping { get { return m_isJumping; } }

    public bool m_isMoving, m_isJumping;
    public float m_newPosX;
    public Vector3 m_newPos;
    public Transform m_MenuBack;

    private float m_delay;
    private MenuChoice m_MenuChoice;
    private bool m_turned;

    Vector3 m_JumpPos;

    // Use this for initialization
    void Start ()
    {
        m_MenuBack = GameObject.Find("tumble dryer").transform;

        if (m_MenuChoice == null)
        {
            m_MenuChoice = m_MenuBack.GetComponent<MenuChoice>();
        }
        if (transform.CompareTag("Menu P1"))
        {
            m_newPos = new Vector3(transform.position.x + 2, transform.position.y - 0.75f, transform.position.z);
            m_isMoving = true;
        }
        else if (transform.CompareTag("Menu P2"))
        {
            m_newPos = new Vector3(transform.position.x + 4.5f, transform.position.y - 1, transform.position.z);
            m_isMoving = true;
        }
        else if (transform.CompareTag("Menu P3"))
        {
            m_newPos = new Vector3(transform.position.x + 8.5f, transform.position.y - 1, transform.position.z);
            m_isMoving = true;
        }
        else if (transform.CompareTag("Menu P4"))
        {
            m_newPos = new Vector3(transform.position.x + 11, transform.position.y - 0.75f, transform.position.z);
            m_isMoving = true;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_MenuChoice.m_StartRun)
        {

            if (transform.CompareTag("Menu P2") || transform.CompareTag("Menu P1"))
            {
                m_newPos = new Vector3(-1.05f, -3.45f, transform.position.z);
            }
            else if (transform.CompareTag("Menu P3") || transform.CompareTag("Menu P4"))
            {
                m_newPos = new Vector3(1.05f, -3.45f, transform.position.z);
            }
            m_isMoving = true;
        }

        if (m_isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_newPos, Time.deltaTime * 1.75f);
        }


        if (transform.position == m_newPos)
        {
            m_isMoving = false;
            if (transform.position.x > 0.2f && !m_turned)
            {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
                m_turned = true;
            }
        }

        if (transform.position == m_newPos && (m_newPos.y == -3.415f || m_newPos.y == -3.45f))
        {
            m_isMoving = false;
            m_isJumping = true;
        }

        if (m_isJumping)
        {
            if (transform.CompareTag("Menu P2") || transform.CompareTag("Menu P1"))
            {
                m_JumpPos = new Vector3(0, -1.5f, transform.position.z);
            }
            else if (transform.CompareTag("Menu P3") || transform.CompareTag("Menu P4"))
            {
                m_JumpPos = new Vector3(0, -1.5f, transform.position.z);
            }
            transform.position = Vector3.MoveTowards(transform.position, m_JumpPos, 0.15f);
        }

        if(transform.position.x == 0  && transform.position.y == -1.5f)
        {
            transform.GetComponent<Renderer>().sortingOrder = -1;
            transform.GetComponent<Renderer>().sortingLayerName = "Default";
        }
    }
}
