﻿using UnityEngine;
using System.Collections;
using Player_Data_Collection;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MenuChoice1 : Player_Controller {

    
    public AudioSource stingSource;


    public Transform m_Basket;// Add/remove players
    public Transform m_Washer;// Options
    public GameObject m_Dryer;// Play
    public Transform m_Door;// Quit
    private Player_ID m_PlayerID;
    private bool lastpressed = false;
    private bool pressed = false;
    public float m_selected = 1;

    public int m_Activated_State;
    public int m_Deactivated_State;

    public Color color = new Color(0.2F, 0.3F, 0.4F, 0.5F);

    // Use this for initialization
    void Start ()
    {
        m_Dryer = GameObject.Find("Tumble Dryer");


        m_Activated_State = Animator.StringToHash("Base Layer.Tumble dryer highlighted");
        m_Deactivated_State = Animator.StringToHash("Base Layer.Dehighlighted");
        m_Dryer.GetComponent<Animator>().Stop();
    }

    // Update is called once per frame
    void Update()
    {
        float axis_Value = get_Horizontal_Axis(m_PlayerID, true);


        if (Mathf.Abs(axis_Value) > 0)
            pressed = true;
        else
            pressed = false;

        
        if (pressed && !lastpressed)
        {
            if (axis_Value < 0.0f)
            {
                m_selected--;
            }
            if (axis_Value > 0.0f)
            {
                m_selected++;
            }
        }
        lastpressed = pressed;

        if (get_Jump_Button(m_PlayerID, Button_State.Button_State_Down))
        {
            if (m_selected == 3)
                SceneManager.LoadScene(1);

        }

        if (m_selected < 1)
            m_selected = 3;
        else if (m_selected > 3)
            m_selected = 1;

        if(m_selected == 1)
        {
            m_Basket.GetComponent<SpriteRenderer>().color = color;
            m_Washer.GetComponent<SpriteRenderer>().color = Color.white;
            m_Dryer.GetComponent<SpriteRenderer>().color = Color.white;
            m_Door.GetComponent<SpriteRenderer>().color = Color.white;
        }
        else if (m_selected == 2)
        {
            m_Basket.GetComponent<SpriteRenderer>().color = Color.white;
            m_Washer.GetComponent<SpriteRenderer>().color = color;
            m_Dryer.GetComponent<SpriteRenderer>().color = Color.white;
            m_Door.GetComponent<SpriteRenderer>().color = Color.white;
        }
        else if (m_selected == 3)
        {
            //m_Dryer.GetComponent<Animator>().Play(m_Activated_State);
            m_Basket.GetComponent<SpriteRenderer>().color = Color.white;
            m_Washer.GetComponent<SpriteRenderer>().color = Color.white;
            m_Dryer.GetComponent<SpriteRenderer>().color = color;
            m_Door.GetComponent<SpriteRenderer>().color = Color.white;
        }
        //else if (m_selected == 4)
        //{
        //    m_Basket.GetComponent<SpriteRenderer>().color = Color.white;
        //    m_Washer.GetComponent<SpriteRenderer>().color = Color.white;
        //    m_Dryer.GetComponent<SpriteRenderer>().color = Color.white;
        //    m_Door.GetComponent<SpriteRenderer>().color = color;
        //}

    }
}
