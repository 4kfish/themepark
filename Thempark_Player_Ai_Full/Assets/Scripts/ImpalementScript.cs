﻿using UnityEngine;
using System.Collections;

//Seperate for arrow and needletrap.

[RequireComponent(typeof(DistanceJoint2D))]
public class ImpalementScript : MonoBehaviour {
    private PolygonCollider2D m_Spike_Collider;
    private DistanceJoint2D m_Distancejoint;
    private Rigidbody2D m_Spike_Rigidbody;
    private Rigidbody2D m_Target_Rigidbody;
    private float m_timeelapsed;
    private bool m_hit_timer;

    // Use this for initialization
    void Start ()
    {
        m_Spike_Collider = GetComponent<PolygonCollider2D>();
        m_Distancejoint = GetComponent<DistanceJoint2D>();
        m_Spike_Rigidbody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Timer to calculate the time since impalement
	    if(m_hit_timer)
        {
            m_timeelapsed += Time.deltaTime;
        }
        //This timer makes the impalement happen after a certain amount of time, which makes the body go into the spikes and then disable the movement
        if(m_timeelapsed > 0.02)
        {
            //ADD DEATH BOOLEAN SO THAT THE PLAYER DIES.
            m_Target_Rigidbody.GetComponent<Player_Movement>().enabled = false;
            m_Target_Rigidbody.isKinematic = true;
            m_Target_Rigidbody.velocity = Vector2.zero;
            //m_Spike_Rigidbody.isKinematic = true;

            m_hit_timer = false;
            m_timeelapsed = 0.0f;
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        //When the colliders hit the target the rotation i z is unlocked and makes the player "limp".
        //It also makes the spike colliders not hit the colliders on the targets body and turns the hit timer boolean to true.
        if (collision.collider.tag != "Arm_Segment")
        {
            m_Target_Rigidbody = collision.transform.GetComponent<Rigidbody2D>();
            //m_Target_Rigidbody.freezeRotation = false;
            //Physics2D.IgnoreCollision(m_Spike_Collider, m_Target_Rigidbody.GetComponent<BoxCollider2D>(), true);
            //Physics2D.IgnoreCollision(m_Spike_Collider, m_Target_Rigidbody.GetComponent<CircleCollider2D>(), true);
        
            m_hit_timer = true;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {

    }
}
