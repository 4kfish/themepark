﻿using UnityEngine;
using System.Collections;
using Player_Data_Collection;
using UnityEngine.SceneManagement;

public class WinnerPlacement : Player_Controller
{
    public Transform[] m_Players;
    public int player_Amount;

    private GameObject m_Placement;
    private GameObject m_Game_Controller;
    private PointCheck m_PointCheckScript;
    private Game_Controller m_Game_Controller_Script;
    private Transform m_Player_Transform;
    private Player_ID_Script m_Player_ID_Script;
    private Player_ID[] m_PlayerIDs;
    private Player_ID m_Player_ID;

    void Start()
    {
        m_Game_Controller = GameObject.Find("Scene");
        m_Game_Controller_Script = m_Game_Controller.GetComponent<Game_Controller>();

        player_Amount = (int)m_Game_Controller_Script.m_Player_Amount;
        m_PlayerIDs = new Player_ID[player_Amount];

        m_Player_ID_Script = GameObject.Find("Player_ID").GetComponent<Player_ID_Script>();
        m_PlayerIDs = m_Player_ID_Script.m_PlayerID;

        //Placera ut PlayerID på sin sorterade plats. PlayerID från player_id_script och plats från samma.
        for (int x = 0; x < player_Amount; x++)
        {
            int i = x + 1;
            int playerID = (int)m_PlayerIDs[x] + 1;

            m_Player_Transform = Instantiate(Resources.Load("MenuSock_" + playerID.ToString(), typeof(Transform))) as Transform;
            m_Placement = GameObject.Find("Place_" + i.ToString());
            m_Player_Transform.position = m_Placement.transform.position;
        }
        Game_Controller gameController = GameObject.Find("Scene").GetComponent<Game_Controller>();
        gameController.gameObject.SetActive(false);
    }

    void FixedUpdate()
    {
        if (get_Interact_Button(m_Player_ID, Button_State.Button_State_Down))
        {
            SceneManager.LoadScene("MenuScene");
        }
    }
}
